# ts\_convert

A script to convert ts-stream to mpg/mp4

## Features

* graphical selection-dialog for choosing the source files
* graphical selection-dialog for choosing the output directory
* Works with SD- and HD-recordings
* Some receivers, like TechniSat when writing to FAT32 and NTFS formatted discs, split recordings into pieces of 1 GB. *ts_convert* detects this and combines automatically all related pieces to one destination file. It is sufficient to choose at least one of the split files.
* The same applies for VOB-files. Just select one of the VTS\_xxxx.VOB-files of the main title.
* If your TV has a built-in UPnP server, it is possible to read directly from your TV over the network. For this the use of djmount is very helpful. *ts_convert* parses the m3u playlist files and reads the ts-file it links to.
* Uses ffmpeg or avconv, whatever is available
* Very fast, as streams are not re-encoded
* Files converted with ts\_convert can later be edited with avidemux, without losing A/V-sync

## Usage

*ts_convert* needs the following programs:

* ffmpeg (or avconv)
* getopt
* zenity

Make sure, they are installed.


    ts_convert  - convert ts files to mpg/mp4
    
    usage: ts_convert [OPTIONS] [video-file|m3u-file …]
    
         OPTIONS:
         -d|--delete       delete source files after conversion [no]
         -h|--help         show this help screen
         -n|--dryrun       do all but create files (for development)
         -o|--outdir dir   use dir as output directory, without asking
         -p|--noprogress   don't show progressbars
         -v|--version      show program name and version, then exit
         -5|--html5        transcode to HTML5-compatible HD-video (h264,aac) (slow)

         defaults are shown in square brackets

## Installation

### Archlinux

1.  `git clone https://git.tuxomat.eu/automat/ts_convert.git`
2.  `cd ts_convert`
3.  `makepkg -sic`

*ts_convert* will be listed in your menu's multimedia-section.

### other distributions

1.  download the file *install\_ts\_convert.sh* from this side and make it executable
2.  be sure to have *curl* and *unzip* installed.
3.  execute `./install_ts_convert`. It downloads the latest stable version and installs it to /usr/local/bin. For this it is required that you have sudo rights.

*ts_convert* will be listed in your menu's multimedia-section.

