#!/bin/bash

declare srcversion='stable'
declare package='ts_convert'
declare src="https://git.tuxomat.eu/automat/$package/archive/$srcversion.zip"
declare -a neededprogs=('curl' 'unzip')
declare tmpdir='/tmp'

function die() {
        echo -e "$*"
        exit 1
}

function look_for_progs() {
        for p in "${neededprogs[@]}"; do
                command -v "$p" >&/dev/null || die "can't find program $p. Please install first"
        done
}

look_for_progs
cd "$tmpdir" || die "can't cd to dir $tmpdir"
echo "downloading $package…"
curl -sLO "$src" || die "can't download $src. Is curl installed?"
echo "extracting $package…"
unzip -q -x "$srcversion.zip" || die "can't unzip $srcversion.zip"
cd "$package" || die "can't cd to $package"
sed -i -e 's|usr/bin|/usr/local/bin|' "$package.desktop"
echo "installing $package…"
sudo install "$package" /usr/local/bin || die "can't install $package to /usr/local/bin"
sudo install -m 644 "$package.desktop" /usr/share/applications || die "can't install $package.desktop to /usr/share/applications"
cd .. && rm -rf "$package" "$srcversion.zip"

echo
echo 'ok, everything is fine'
echo 'You find ts_convert in your menu in the multimedia-section'
echo
echo 'see ts_convert -h if you want to know more'
